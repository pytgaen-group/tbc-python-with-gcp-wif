from google.cloud import bigquery, storage


def list_buckets():
    """Lists all buckets."""

    storage_client = storage.Client()
    return list(storage_client.list_buckets())


def list_datasets(project: str = "bigquery-public-data") -> None:
    """Lists all dataset. (public dataset by default)"""

    client = bigquery.Client()
    project = "bigquery-public-data"

    return [d.full_dataset_id for d in client.list_datasets(project=project)]


def main():
    lsd = list_datasets()  # free operation
    print(lsd)
    # list_buckets()  # not free operation


if __name__ == "__main__":
    main()
