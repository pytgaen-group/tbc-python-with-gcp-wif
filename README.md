# Python GCP Variant project sample

This project sample shows the usage of _to be continuous_ templates:

* Python

The project get the list of datasets in `bigquery-public-data` Bigquery Project.
The program get the credential with [Authenticate for using client libraries](https://cloud.google.com/docs/authentication/client-libraries) with [ADC](https://cloud.google.com/docs/authentication/application-default-credentials)

[Detailed article on internal OIDC impersonated with Workload Identify Federation](https://blog.salrashid.dev/articles/2021/understanding_workload_identity_federation/#oidc-impersonated)

## Requirement for Workload Identity Federation Pool (WIF)  

List of requirements before using this variant for use Python Google Clients:

1. You must have a Workload Identity Federation Pool, (Help [Create and configure a workload identity federation with Gcloud CLI](https://docs.gitlab.com/ee/integration/google_cloud_iam.html#with-the-google-cloud-cli
))
2. You must have a Service Account with enough permissions to run your python job.
3. You must allow WIF impersonation to the Service Account ([Google Doc](https://cloud.google.com/iam/docs/workload-identity-federation#impersonation))

The here a full sample with gcloud CLI to make a config

### Template configuration

Define this variables in Settings > CI/CD > Variables or less recommanded, in file `.gitlab-ci.yml`

* `GCP_OIDC_ACCOUNT`: "<sa-for-cicd-XZYA@yyyy.iam.gserviceaccount.com>"
* `GCP_OIDC_PROVIDER`: "projects/0000000/locations/global/workloadIdentityPools/XYZB-pool/providers/gitlab-XYZC"

Optional, you can define variable `GOOGLE_CLOUD_PROJECT` to define the default Google project.
So in this code below with where create a Client with no parameter, the log will go to `GOOGLE_CLOUD_PROJECT`

```python
# Imports the Cloud Logging client library
import google.cloud.logging

# Instantiates a client
client = google.cloud.logging.Client()
```

## Python template features

This project uses the following features from the GitLab CI Python template:

* Uses the `pyproject.toml` build specs file with [Poetry](https://python-poetry.org/) as build backend,
* Enables the [pytest](https://docs.pytest.org/) unit test framework by declaring the `$PYTEST_ENABLED` in the `.gitlab-ci.yml` variables,
* Enables the [ruff](https://docs.astral.sh/ruff/) linter by declaring the `$RUFF_ENABLED` in the `.gitlab-ci.yml` variables and define `ruff.toml` file

The Python template also enforces:

* [test report integration](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html),
* [code quality report integration](https://docs.gitlab.com/ee/ci/testing/code_quality.html),
* and [code coverage integration](https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-report-badge).

## Ruff linter features

[Ruff](https://docs.astral.sh/ruff/) can replace many linter with better performance. In this sample it replace Flake8, Bandit, isort, ...
In this sample we define file `ruff.toml` to:

* activate Flake8, Bandit, isort, ...
* Ignore `S101` (Use of \`assert\` detected) in tests directory
  
For reference:

* [Ruff rules list](https://docs.astral.sh/ruff/rules/)
* [disable QA one by one in Python source code](https://docs.astral.sh/ruff/linter/#error-suppression)

## Example to create Workload Identity Federation with CLI

```bash

export PROJECT_NAME="my_gcp_prject"
export PROJECT_NUMBER=$(gcloud projects describe "${PROJECT_NAME}" --format='value(projectNumber)')
export SERVICE_ACCOUNT_NAME="sa-for-cicd-gitlab"
export SERVICE_ACCOUNT_EMAIL="${SERVICE_ACCOUNT_NAME}@${PROJECT_NAME}.iam.gserviceaccount.com"
export POOL_ID="sa-impersonate-wif-pool"
export GITLAB_PROJECT_ID=12345678
export GITLAB_NAMESPACE_PATH="https://gitlab.com/my_group/my_subgroup)"

gcloud iam workload-identity-pools create "${POOL_ID}" \
--location="global" \
--project ${PROJECT_NUMBER} \
--description="to use in Gitlab CICD demo" \
--display-name="Gitlab CICD demo"

gcloud iam workload-identity-pools providers create-oidc sa-gitlab-cicd-provider \
--location="global" \
--project ${PROJECT_NUMBER} \
--display-name="Gitlab CICD demo" \
--workload-identity-pool="${POOL_ID}" \
--issuer-uri="https://gitlab.com" \
--allowed-audiences="https://gitlab.com" \
--attribute-mapping="google.subject=assertion.project_id,attribute.project_id=assertion.project_id,attribute.user_email=assertion.user_email,attribute.user_login=assertion.user_login,attribute.ref=assertion.ref,attribute.ref_type=assertion.ref_type,attribute.ref_protected=assertion.ref_protected,attribute.environment=assertion.environment" \
--attribute-condition="attribute.project_id in ['${GITLAB_PROJECT_ID}']"

gcloud iam service-accounts create ${SERVICE_ACCOUNT_NAME} \
--display-name="Service account used by WIF in Gitlab CICD" \
--project ${PROJECT_NUMBER}

gcloud iam service-accounts add-iam-policy-binding ${SERVICE_ACCOUNT_EMAIL}  \
--role=roles/iam.workloadIdentityUser  \
--member="principalSet://iam.googleapis.com/projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/*"

```

## Other condition for provider

```bash 
--attribute-condition="assertion.namespace_path.startsWith(\"$GITLAB_NAMESPACE_PATH\")" \
```

## Other condition for policy binding

Look [Google Doc](https://cloud.google.com/iam/docs/workload-identity-federation#impersonation)

```bash
SUBJECT="attribute.project_id/${GITLAB_PROJECT_ID}"
gcloud iam service-accounts add-iam-policy-binding ${SERVICE_ACCOUNT_EMAIL} \
--role=roles/iam.workloadIdentityUser \
--member="principal://iam.googleapis.com/projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/subject/${SUBJECT}"
```

Some read:
* [GitLab WIF - Doit](https://engineering.doit.com/secure-access-to-gcp-services-in-gitlab-pipelines-with-workload-identity-federation-7e12f3d17114/)
* [GitLab WIF - Medium](https://medium.com/google-cloud/gitlab-and-workload-identity-federation-on-google-cloud-a0795091e404)
* [GitLab WIF - Revolve](https://blog.revolve.team/2023/02/28/cicd-without-credentials/)
